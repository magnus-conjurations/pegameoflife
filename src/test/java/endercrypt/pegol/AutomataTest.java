package endercrypt.pegol;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.pegol.assist.AutomataTestAssistant;
import endercrypt.pegol.automata.AutomataConfiguration;
import endercrypt.pegol.automata.AutomataMap;

import org.junit.jupiter.api.Test;


public class AutomataTest
{
	@Test
	void testConfiguration()
	{
		assertThrows(IllegalArgumentException.class, () -> {
			new AutomataConfiguration.Builder()
				.setHeight(5)
				.build();
		}, "width must be set to a positive value");
		
		assertThrows(IllegalArgumentException.class, () -> {
			new AutomataConfiguration.Builder()
				.setWidth(5)
				.build();
		}, "height must be set to a positive value");
		
		assertThrows(IllegalArgumentException.class, () -> {
			new AutomataConfiguration.Builder()
				.setWidth(5)
				.setHeight(5)
				.build();
		}, "Ruleset must be set");
	}
	
	@Test
	void testCellEquals()
	{
		AutomataMap map1 = AutomataTestAssistant.generate(""
			+ "..X\n"
			+ "..X");
		
		AutomataMap map2 = AutomataTestAssistant.generate(""
			+ "..X\n"
			+ "X..");
		
		assertEquals(map1.getCell(0, 0), map2.getCell(0, 0));
		assertEquals(map1.getCell(2, 0), map2.getCell(2, 0));
		assertNotEquals(map1.getCell(2, 1), map2.getCell(2, 1));
		assertNotEquals(map1.getCell(2, 0), map2.getCell(0, 1));
		assertNotEquals(map1.getCell(1, 0), map2.getCell(1, 1));
	}
	
	@Test
	void testMapEquals()
	{
		AutomataMap mapCorrect1 = AutomataTestAssistant.generate(""
			+ "..X");
		
		AutomataMap mapCorrect2 = AutomataTestAssistant.generate(""
			+ "..X");
		
		assertEquals(mapCorrect1.hashCode(), mapCorrect2.hashCode());
		assertEquals(mapCorrect1, mapCorrect2);
		
		AutomataMap mapIncorrect = AutomataTestAssistant.generate(""
			+ ".X.");
		
		assertNotEquals(mapCorrect1.hashCode(), mapIncorrect.hashCode());
		assertNotEquals(mapCorrect1, mapIncorrect);
	}
	
	@Test
	void testMapAsciiArt()
	{
		AutomataMap map = AutomataTestAssistant.generate(""
			+ "...\n"
			+ ".X.\n"
			+ "...");
		
		assertEquals(""
			+ "[ ][ ][ ]\n"
			+ "[ ][x][ ]\n"
			+ "[ ][ ][ ]", map.toString());
	}
}
