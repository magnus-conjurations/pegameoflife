package endercrypt.pegol;


import endercrypt.pegol.assist.AutomataTestAssistant;
import endercrypt.pegol.assist.GameOfLifeTestAssistant;
import endercrypt.pegol.automata.AutomataMap;

import org.junit.jupiter.api.Test;


/**
 * https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Examples_of_patterns
 */
public class GameOfLifePatternTest
{
	@Test
	void testBlinker()
	{
		AutomataMap frame1 = AutomataTestAssistant.generate(""
			+ "....\n"
			+ ".XX.\n"
			+ ".XX.\n"
			+ "....");
		
		GameOfLifeTestAssistant.testRepeatingSequence(frame1);
	}
	
	@Test
	void testToad()
	{
		AutomataMap frame1 = AutomataTestAssistant.generate(""
			+ "......\n"
			+ "......\n"
			+ "..XXX.\n"
			+ ".XXX..\n"
			+ "......\n"
			+ "......");
		
		AutomataMap frame2 = AutomataTestAssistant.generate(""
			+ "......\n"
			+ "...X..\n"
			+ ".X..X.\n"
			+ ".X..X.\n"
			+ "..X...\n"
			+ "......");
		
		GameOfLifeTestAssistant.testRepeatingSequence(frame1, frame2);
	}
	
	@Test
	void testBeacon()
	{
		AutomataMap frame1 = AutomataTestAssistant.generate(""
			+ "......\n"
			+ ".XX...\n"
			+ ".XX...\n"
			+ "...XX.\n"
			+ "...XX.\n"
			+ "......");
		
		AutomataMap frame2 = AutomataTestAssistant.generate(""
			+ "......\n"
			+ ".XX...\n"
			+ ".X....\n"
			+ "....X.\n"
			+ "...XX.\n"
			+ "......");
		
		GameOfLifeTestAssistant.testRepeatingSequence(frame1, frame2);
	}
	
	@Test
	void testGlider()
	{
		AutomataMap frame1 = AutomataTestAssistant.generate(""
			+ ".......\n"
			+ "...X...\n"
			+ ".X.X...\n"
			+ "..XX...\n"
			+ ".......\n"
			+ ".......");
		
		AutomataMap frame2 = AutomataTestAssistant.generate(""
			+ ".......\n"
			+ "..X....\n"
			+ "...XX..\n"
			+ "..XX...\n"
			+ ".......\n"
			+ ".......");
		
		AutomataMap frame3 = AutomataTestAssistant.generate(""
			+ ".......\n"
			+ "...X...\n"
			+ "....X..\n"
			+ "..XXX..\n"
			+ ".......\n"
			+ ".......");
		
		AutomataMap frame4 = AutomataTestAssistant.generate(""
			+ ".......\n"
			+ ".......\n"
			+ "..X.X..\n"
			+ "...XX..\n"
			+ "...X...\n"
			+ ".......");
		
		AutomataMap frame5 = AutomataTestAssistant.generate(""
			+ ".......\n"
			+ ".......\n"
			+ "....X..\n"
			+ "..X.X..\n"
			+ "...XX..\n"
			+ ".......");
		
		GameOfLifeTestAssistant.testSequence(frame1, frame2, frame3, frame4, frame5);
	}
}
