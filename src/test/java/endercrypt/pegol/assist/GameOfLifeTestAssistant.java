package endercrypt.pegol.assist;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.pegol.automata.AutomataCell;
import endercrypt.pegol.automata.AutomataConfiguration;
import endercrypt.pegol.automata.AutomataFrame;
import endercrypt.pegol.automata.AutomataMap;
import endercrypt.pegol.automata.AutomataRuleSet;

import java.util.Arrays;
import java.util.Iterator;


public class GameOfLifeTestAssistant
{
	public static AutomataFrame generateBasicInitialFrame(AutomataMap map)
	{
		AutomataConfiguration configuration = new AutomataConfiguration.Builder()
			.setRuleset(AutomataRuleSet.gameOfLife)
			.setWidth(map.getWidth())
			.setHeight(map.getHeight())
			.build();
		
		AutomataFrame frame = configuration.createBlankFrame();
		
		for (AutomataCell cell : map)
		{
			frame
				.getMap()
				.getCell(cell.getX(), cell.getY())
				.setAlive(cell.isAlive());
		}
		
		return frame;
	}
	
	public static AutomataFrame testSequence(AutomataMap... sequence)
	{
		Iterator<AutomataMap> iterator = Arrays.asList(sequence).iterator();
		if (iterator.hasNext() == false)
		{
			throw new IllegalArgumentException("no sequence specified");
		}
		AutomataFrame frame = generateBasicInitialFrame(iterator.next());
		
		int index = 1;
		while (iterator.hasNext())
		{
			index++;
			frame = frame.advance();
			
			AutomataMap nextMap = iterator.next();
			
			assertEquals(nextMap, frame.getMap(), "sequence: " + index);
		}
		
		return frame;
	}
	
	public static AutomataFrame testRepeatingSequence(AutomataMap... sequence)
	{
		if (sequence.length == 0)
		{
			throw new IllegalArgumentException("no sequence specified");
		}
		AutomataMap ending = sequence[0];
		
		AutomataFrame frame = testSequence(sequence);
		frame = frame.advance();
		
		assertEquals(ending, frame.getMap(), "repeating");
		
		return frame;
	}
}
