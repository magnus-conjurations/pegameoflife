package endercrypt.pegol.assist;


import endercrypt.pegol.automata.AutomataMap;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class AutomataTestAssistant
{
	public static AutomataMap generate(String state)
	{
		List<String> lines = Arrays.asList(state.split("\n"));
		if (state.length() == 0)
		{
			throw new IllegalArgumentException("no lines present (0 height)");
		}
		int height = lines.size();
		
		Set<Integer> widths = lines
			.stream()
			.map(String::length)
			.collect(Collectors.toSet());
		
		if (widths.size() != 1)
		{
			throw new IllegalArgumentException("ambigious map width " + widths);
		}
		
		int width = widths.iterator().next();
		
		AutomataMap map = new AutomataMap(width, height);
		for (int y = 0; y < lines.size(); y++)
		{
			String line = lines.get(y);
			for (int x = 0; x < width; x++)
			{
				boolean alive = resolve(line.charAt(x));
				map.getCell(x, y).setAlive(alive);
			}
		}
		return map;
	}
	
	private static boolean resolve(char c)
	{
		switch (c)
		{
		case 'X':
			return true;
		case '.':
			return false;
		default:
			throw new IllegalArgumentException("unknown symbol: " + c);
		}
	}
}
