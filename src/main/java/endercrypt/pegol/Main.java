package endercrypt.pegol;


import endercrypt.pegol.automata.AutomataConfiguration;
import endercrypt.pegol.automata.AutomataFrame;
import endercrypt.pegol.automata.AutomataMap;
import endercrypt.pegol.automata.AutomataRuleSet;


/**
 * Sample main class demonstrating simple usage of the Cellular automata
 */
public class Main
{
	public static void main(String[] args) throws InterruptedException
	{
		AutomataConfiguration configuration = new AutomataConfiguration.Builder()
			.setRuleset(AutomataRuleSet.gameOfLife)
			.setWidth(3)
			.setHeight(3)
			.build();
		
		AutomataFrame frame = configuration.createBlankFrame();
		AutomataMap map = frame.getMap();
		map.getCell(1, 0).setAlive(true);
		map.getCell(1, 1).setAlive(true);
		map.getCell(1, 2).setAlive(true);
		
		while (true)
		{
			System.out.println(""
				+ "-".repeat(10) + "\n"
				+ frame.getMap());
			
			Thread.sleep(1000);
			frame = frame.advance();
		}
	}
}
