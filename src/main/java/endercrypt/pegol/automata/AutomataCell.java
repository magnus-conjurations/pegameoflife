package endercrypt.pegol.automata;


import java.util.ArrayList;
import java.util.Collection;


public class AutomataCell
{
	public static final String asciiAlive = "[x]";
	public static final String asciiDead = "[ ]";
	
	private final AutomataMap map;
	private final int x;
	private final int y;
	private boolean alive;
	
	protected AutomataCell(AutomataMap map, int x, int y, boolean alive)
	{
		this.map = map;
		this.x = x;
		this.y = y;
		this.alive = alive;
	}
	
	public AutomataMap getMap()
	{
		return map;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public void setAlive(boolean alive)
	{
		this.alive = alive;
	}
	
	public boolean isAlive()
	{
		return alive;
	}
	
	public Collection<AutomataCell> getNeighbours()
	{
		Collection<AutomataCell> neighbours = new ArrayList<AutomataCell>();
		for (int rx = -1; rx <= 1; rx++)
		{
			for (int ry = -1; ry <= 1; ry++)
			{
				boolean self = (rx == 0) && (ry == 0);
				if (self == false)
				{
					getMap()
						.getOptionalCell(x + rx, y + ry)
						.ifPresent(neighbours::add);
				}
			}
		}
		return neighbours;
	}
	
	public int countLivingNeighbours()
	{
		return (int) getNeighbours()
			.stream()
			.filter(AutomataCell::isAlive)
			.count();
	}
	
	public String asAscii()
	{
		return isAlive() ? asciiAlive : asciiDead;
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + (alive ? 1231 : 1237);
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (!(obj instanceof AutomataCell)) return false;
		AutomataCell other = (AutomataCell) obj;
		if (alive != other.alive) return false;
		if (x != other.x) return false;
		if (y != other.y) return false;
		return true;
	}
	
	@Override
	public String toString()
	{
		return "AutomataCell [x=" + x + ", y=" + y + ", alive=" + alive + "]";
	}
}
