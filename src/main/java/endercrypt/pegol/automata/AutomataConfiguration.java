package endercrypt.pegol.automata;

public final class AutomataConfiguration
{
	private final int width;
	private final int height;
	private final AutomataRuleSet ruleSet;
	
	private AutomataConfiguration(Builder builder)
	{
		width = builder.getWidth();
		height = builder.getHeight();
		ruleSet = builder.getRuleset();
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public AutomataRuleSet getRuleSet()
	{
		return ruleSet;
	}
	
	public AutomataFrame createBlankFrame()
	{
		return new AutomataFrame(this, 0L, new AutomataMap(getWidth(), getHeight()));
	}
	
	public static class Builder
	{
		private int width = 0;
		
		public Builder setWidth(int width)
		{
			this.width = width;
			return this;
		}
		
		private int getWidth()
		{
			if (width <= 0)
			{
				throw new IllegalArgumentException("width must be set to a positive value");
			}
			return width;
		}
		
		private int height;
		
		public Builder setHeight(int height)
		{
			this.height = height;
			return this;
		}
		
		private int getHeight()
		{
			if (height <= 0)
			{
				throw new IllegalArgumentException("height must be set to a positive value");
			}
			return height;
		}
		
		private AutomataRuleSet ruleset;
		
		public Builder setRuleset(AutomataRuleSet ruleset)
		{
			this.ruleset = ruleset;
			return this;
		}
		
		private AutomataRuleSet getRuleset()
		{
			if (ruleset == null)
			{
				throw new IllegalArgumentException("Ruleset must be set");
			}
			return ruleset;
		}
		
		public AutomataConfiguration build()
		{
			return new AutomataConfiguration(this);
		}
	}
}
