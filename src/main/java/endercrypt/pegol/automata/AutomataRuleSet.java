package endercrypt.pegol.automata;

public interface AutomataRuleSet
{
	public static final AutomataRuleSet gameOfLife = new AutomataRuleSet()
	{
		@Override
		public boolean shouldSpawn(AutomataCell cell)
		{
			int neighbours = cell.countLivingNeighbours();
			return neighbours == 3;
		}
		
		@Override
		public boolean shouldDie(AutomataCell cell)
		{
			int neighbours = cell.countLivingNeighbours();
			return neighbours < 2 || neighbours > 3;
		}
	};
	
	public boolean shouldSpawn(AutomataCell cell);
	
	public boolean shouldDie(AutomataCell cell);
}
