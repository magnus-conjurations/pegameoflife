package endercrypt.pegol.automata;

public class AutomataFrame
{
	private final AutomataConfiguration configuration;
	private final long frame;
	private final AutomataMap map;
	
	protected AutomataFrame(AutomataConfiguration configuration, long frame, AutomataMap map)
	{
		this.configuration = configuration;
		this.frame = frame;
		this.map = map;
	}
	
	public AutomataConfiguration getConfiguration()
	{
		return configuration;
	}
	
	public long getFrame()
	{
		return frame;
	}
	
	public AutomataMap getMap()
	{
		return map;
	}
	
	public AutomataFrame advance()
	{
		int width = configuration.getWidth();
		int height = configuration.getHeight();
		AutomataMap nextMap = new AutomataMap(width, height);
		for (AutomataCell cell : map)
		{
			if (isShouldCellLive(cell))
			{
				nextMap
					.getCell(cell.getX(), cell.getY())
					.setAlive(true);
			}
		}
		return new AutomataFrame(configuration, frame + 1, nextMap);
	}
	
	private boolean isShouldCellLive(AutomataCell cell)
	{
		if (cell.isAlive())
		{
			if (configuration.getRuleSet().shouldDie(cell) == false)
			{
				return true;
			}
		}
		else
		{
			if (configuration.getRuleSet().shouldSpawn(cell))
			{
				return true;
			}
		}
		return false;
	}
}
