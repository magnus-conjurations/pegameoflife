package endercrypt.pegol.automata;


import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;


public class AutomataMap implements Iterable<AutomataCell>
{
	private final int width;
	private final int height;
	private final AutomataCell[][] cells;
	
	public AutomataMap(int width, int height)
	{
		this.width = width;
		this.height = height;
		
		cells = new AutomataCell[width][height];
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				cells[x][y] = new AutomataCell(this, x, y, false);
			}
		}
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public boolean isInRange(int x, int y)
	{
		return x >= 0 && y >= 0 && x < width && y < height;
	}
	
	public Optional<AutomataCell> getOptionalCell(int x, int y)
	{
		if (isInRange(x, y) == false)
		{
			return Optional.empty();
		}
		return Optional.of(cells[x][y]);
	}
	
	public AutomataCell getCell(int x, int y)
	{
		return getOptionalCell(x, y)
			.orElseThrow(() -> new ArrayIndexOutOfBoundsException("cell " + x + "," + y + " out of range on map with size " + width + "," + height));
	}
	
	@Override
	public Iterator<AutomataCell> iterator()
	{
		return new Iterator<AutomataCell>()
		{
			private int x = 0;
			private int y = 0;
			
			@Override
			public boolean hasNext()
			{
				return (y < height) && (x < width);
			}
			
			@Override
			public AutomataCell next()
			{
				AutomataCell cell = getOptionalCell(x, y).orElseThrow(NoSuchElementException::new);
				x++;
				if (x >= width)
				{
					x = 0;
					y++;
				}
				return cell;
			}
		};
	}
	
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				String graphics = getCell(x, y).asAscii();
				builder.append(graphics);
			}
			builder.append('\n');
		}
		if (builder.length() > 0 && builder.charAt(builder.length() - 1) == '\n')
		{
			builder.setLength(builder.length() - 1);
		}
		return builder.toString();
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + width;
		result = prime * result + height;
		result = prime * result + Arrays.deepHashCode(cells);
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (!(obj instanceof AutomataMap)) return false;
		AutomataMap other = (AutomataMap) obj;
		if (width != other.width) return false;
		if (height != other.height) return false;
		if (!Arrays.deepEquals(cells, other.cells)) return false;
		return true;
	}
}
